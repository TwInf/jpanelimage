package de.twyman.malen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/*
 * ToDO
 * paramter testing, 
 *   throw appropriate exceptions
 *   especially for the arrays that are provided.  
 *   Ensure the all have the correct dimensions in all areas.
 * documentation 
 * a test harness
 */

/**
 *
 * @author dominic
 */
public class JPanelImage extends JPanel {
    
    private BufferedImage Image;
    private int Skalierung;
    private int iImageType;
    
    public int getImageType() {
        return Image.getType();
    }
    
    public void setGrayScale() {
        if (Image.getType()==BufferedImage.TYPE_BYTE_GRAY)
            return;
        iImageType = BufferedImage.TYPE_BYTE_GRAY;
        BufferedImage Image = new BufferedImage(
                                    this.Image.getWidth()
                                   ,this.Image.getHeight()
                                   ,iImageType);
        
        Graphics g = Image.getGraphics();
        g.drawImage(this.Image, 0, 0, null);
        g.dispose();
        this.Image.flush();
        this.Image=Image;
        repaint();
//        paintComponent(this.getGraphics());
    }
    
    // Zeichne eine Histogramm, Bildgroße wird
    // auf das Panel angepasst.
    public void zeichneHistogramm(int [] Datensatz) {
        if (null==Datensatz)
            return;
        if (0==Datensatz.length)
            return;
        
        int meinWidth = this.getWidth();
        int meinHeight = this.getHeight()-10;
        
        int intBreiteschritte = meinWidth/Datensatz.length;
        //TODO
        // if der Breite unter 1 Pixel ist.. dann muss der Reihung
        // neu summiert und skaliert werden
        
        // a - 300
        // c - 1600
        // 
        if (0==intBreiteschritte) {
            int SpaltenZusammen= Datensatz.length/meinWidth+1;
            int[] Datensatz2 = new int[Datensatz.length/SpaltenZusammen];
            for(int a=0;a<Datensatz2.length;a++) {
                Datensatz2[a/SpaltenZusammen] += Datensatz[a];
            }
            Datensatz = Datensatz2;
            intBreiteschritte = meinWidth/Datensatz.length;
        }
        
        int max = Integer.MIN_VALUE;
        int min = 0;  // wir wollen immer von 0 anfangen falls alles positive ist
        for(int wert: Datensatz) {
            max = Math.max(max, wert);
            min = Math.min(min, wert);
        }
        double iHoeheSchritte = meinHeight/(max*1.0-min);
        int iZeroPos = (int)(iHoeheSchritte*max) ;
        
        BufferedImage Image = new BufferedImage(
                                    meinWidth
                                   ,meinHeight
                                   ,iImageType);
        Graphics g = Image.getGraphics();
        g.setColor(this.getBackground());
        g.fillRect(0, 0, meinWidth, meinHeight);
        g.setColor(Color.BLACK);
        int xPos=0;
        int yPos=0;
        for(int Wert:Datensatz) {
            yPos=Math.min((int)(iHoeheSchritte*(max-Wert)),iZeroPos);
            int iHoehe = (int)(Math.abs(Wert)*iHoeheSchritte);
            g.fillRect(xPos, yPos, intBreiteschritte+1, iHoehe+1);
            xPos+=intBreiteschritte;
        }
        g.dispose();
        if(null!= this.Image)
            this.Image.flush();
        this.Image=Image;
        repaint();
    }
    
    public void setRgb() {
        if (Image.getType()==BufferedImage.TYPE_INT_RGB)
            return;
        iImageType = BufferedImage.TYPE_INT_RGB;
        BufferedImage Image = new BufferedImage(
                                    this.Image.getWidth()
                                   ,this.Image.getHeight()
                                   ,iImageType);
        
        Graphics g = Image.getGraphics();
        g.drawImage(this.Image, 0, 0, null);
        g.dispose();
        if(null!= this.Image)
            this.Image.flush();
        this.Image=Image;
        repaint();
//        paintComponent(this.getGraphics());
    }
    
    public JPanelImage() {
        super();
        this.Image=null;
        Skalierung = 1;
        iImageType = BufferedImage.TYPE_INT_RGB;
    }
    
    public int getSkalierung() {
        return Skalierung;
    }
    
    public void setSkalierung(int Skalierung) {
        if (Skalierung >0)
        {
            if (this.Image != null) {
                Color[][] imgReihung = this.getColorReihung();
                this.Skalierung = Skalierung;
                this.setColorReihung(imgReihung);
                repaint();
//        paintComponent(this.getGraphics());
            }
            else
                this.Skalierung = Skalierung;
        }
    }

    private boolean istReihungGueltig(int[][][] imgReihung) {
        return true;
    }

    private boolean istReihungGueltig(Color[][] imgReihung) {
        return true;
        
    }
    
    public int[][][] getReihung() {
        if (Image == null)
            return null;
        int maxX = Image.getWidth()/Skalierung;
        int maxY = Image.getHeight()/Skalierung;
        int imgReihung[][][] = new int[maxX][maxY][3];
        for (int x = 0;  x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                int rgb = Image.getRGB(x*Skalierung
                                     , y*Skalierung);
                Color c = new Color(rgb);
                imgReihung[x][y][0] = c.getRed();
                imgReihung[x][y][1] = c.getGreen();
                imgReihung[x][y][2] = c.getBlue();
            }
        }
        return imgReihung;
    }
    
    public Color [][] getColorReihung() {
        if (Image == null)
            return null;
        int maxX = Image.getWidth()/this.Skalierung;
        int maxY = Image.getHeight()/this.Skalierung;
        Color imgReihung[][] = new Color[maxX][maxY];
        for (int x = 0;  x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                imgReihung[x][y] = 
                    new Color(Image.getRGB(x*this.Skalierung
                                          ,y*this.Skalierung));
            }
        }
        return imgReihung;
    }

    public int [][] getGrauReihung() {
        if (this.Image == null)
            return null;
        BufferedImage Image2;
        if (this.Image.getType()!=BufferedImage.TYPE_BYTE_GRAY) {
            // erstelle eine Grau Bild
            Image2 = new BufferedImage( this.Image.getWidth()
                                       ,this.Image.getHeight()
                                       ,BufferedImage.TYPE_BYTE_GRAY);

            Graphics g = Image2.getGraphics();
            g.drawImage(this.Image, 0, 0, null);
            g.dispose();
        }
        else {
            Image2 = Image; //ist schon Grau
        }
            
        int maxX = Image2.getWidth()/this.Skalierung;
        int maxY = Image2.getHeight()/this.Skalierung;
        int imgReihung[][] = new int[maxX][maxY];
        for (int x = 0;  x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                Color c = new Color(Image2.getRGB(x*this.Skalierung
                                   ,y*this.Skalierung));
                imgReihung[x][y] = c.getBlue();
            }
        }
        return imgReihung;
    }
    
    public void setReihung(int[][][] imgReihung) {
        if (imgReihung == null) {
            Image = null;
            return;
        }
        int maxX = imgReihung.length;
        int maxY  = imgReihung[0].length;
        BufferedImage Image= new BufferedImage(maxX*Skalierung,maxY*Skalierung,iImageType);
        Graphics2D g2 = Image.createGraphics();
        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                int rot  =imgReihung[x][y][0];
                int gruen=imgReihung[x][y][1];
                int blau =imgReihung[x][y][2];
                g2.setColor(new Color(rot,gruen,blau));
                g2.fillRect(x*this.Skalierung, y*this.Skalierung
                           ,this.Skalierung,   this.Skalierung);
            }
        }
        g2.dispose();
        if(null!= this.Image)
            this.Image.flush();
        this.Image=Image;
        repaint();
//        paintComponent(this.getGraphics());

    }
    
    public void setColorReihung(Color[][] imgReihung) {
        if (imgReihung == null) {
            Image = null;
            return;
        }
        int maxX = imgReihung.length;
        int maxY = imgReihung[0].length;
        BufferedImage Image= new BufferedImage(maxX*Skalierung,maxY*Skalierung,iImageType);
        Graphics2D g2 = Image.createGraphics();
        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                g2.setColor(imgReihung[x][y]);
                g2.fillRect(x*this.Skalierung, y*this.Skalierung
                           ,this.Skalierung,   this.Skalierung);
            }
        }
        g2.dispose();
        if(null!= this.Image)
            this.Image.flush();
        this.Image=Image;
//        repaint();
        // so ist es möglich die Mal-prozess ständig anzuschauen
        // und z.B. eine Fill-algorithmus bei der Arbeit zu vervolgen.
        paintComponent(this.getGraphics());
    }
    
    public void setGrauReihung(int[][] imgReihung) {
        if (imgReihung == null) {
            Image = null;
            return;
        }
        int maxX = imgReihung.length;
        int maxY = imgReihung[0].length;
        BufferedImage Image= new BufferedImage(maxX*Skalierung,maxY*Skalierung,iImageType);
        Graphics2D g2 = Image.createGraphics();
        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                Color c = new Color(imgReihung[x][y]
                                   ,imgReihung[x][y]
                                   ,imgReihung[x][y]);
                g2.setColor( c );
                g2.fillRect(x*this.Skalierung, y*this.Skalierung
                           ,this.Skalierung,   this.Skalierung);
            }
        }
        g2.dispose();
        if(null!= this.Image)
            this.Image.flush();
        this.Image=Image;
        repaint();
//        paintComponent(this.getGraphics());
    }
    
    public BufferedImage getImage() {
        return Image;
    }

    public void setImage(BufferedImage Image) {
        this.Image = Image;
        if (this.Skalierung != 1) {
            int oldSkalierung = Skalierung;
            this.Skalierung = 1;
            setSkalierung(oldSkalierung);
        }
        repaint();
//        paintComponent(this.getGraphics());
    }

    public void setImage(File FileName) {
        try {
            Image = ImageIO.read(FileName);
            if (this.Skalierung != 1) {
                int oldSkalierung = Skalierung;
                this.Skalierung = 1;
                setSkalierung(oldSkalierung);
            }
            repaint();
//        paintComponent(this.getGraphics());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            g.drawImage(Image, 5  , 5, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
